#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>

void sig_handler(int signum){
	
	
    if (signum == SIGCHLD) {
		printf("Handler SIGCHLD\n");
		wait(0);
	}

    return;
}
/*---------------------------------------------------------------------*/
int main(void){    
	int i;
	
	signal(SIGCHLD,sig_handler);
	
    if (fork()==0) {
        printf("    Estoy en proceso hijo\n");
        sleep(30);
        printf("    Proceso Hijo Cerrado\n");
    }
    else {
        printf("Estoy en proceso Padre\n");
        sleep(5);
        printf("Proceso Padre Cerrado\n");
    }
        
    return 0;
}