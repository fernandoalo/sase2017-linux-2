#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#include <time.h>
#include <sys/wait.h>
#include <sys/types.h>

int main(void) { 
	pid_t idHijo;

	idHijo = fork(); //Se crea un proceso 'hijo'

	if (idHijo == 0)  //Devuelve 0 en el 'hijo'
		printf("Soy el hijo!!!! %i\n",(int) getpid()); 
	else
		printf("Soy el padre!!! %i\n",(int) getpid());
		
	return 0;
}