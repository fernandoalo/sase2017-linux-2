/***************************************************************************************************/
/* En este ejemplo se puede ver desde la consola, (previo al getchar()) que no hay señales tomadas */
/*                                                                                                 */
/* Luego del getchar, se puede ver que ya está tomada la señal SIGUSR1 con valor 10 (0x000200)     */
/*                                                                                                 */
/* y por ultimo, se le puede enviar la señal SIGUSR1, para ver como responde                       */
/*                                                                                                 */
/*                                                                                                 */
/* Para ver que señales tiene tomadas se puede ejecutar                                            */
/*     ps -s <pid>                                                                                 */
/*     ps -o pid,ppid,tname,stat,pending,ignored,caught,comm <pid>                                 */
/*                                                                                                 */
/* Para mandar una señal desde otra consola se utiliza el comando kill                             */
/*     kill -<SIGNAL_NAME> <pid>                                                                   */
/*     kill -s <SIGNAL_NAME/SIGNAL_NUMBER> <pid>                                                   */
/*                                                                                                 */
/***************************************************************************************************/

#include <stdio.h>
#include <signal.h>
#include <unistd.h>

void sig_handler(int signum) {
    //if (signum == SIGUSR1)
	printf("Recibida señal SIGUSR1\n");

	return;
}

int main(void){
	printf("PID  = %i\n",(int)getpid());

	getchar();
	signal(SIGUSR1, sig_handler);
	
	printf("Esperando señal\n");
	while(1) sleep(1);
    
    return 0;
}
