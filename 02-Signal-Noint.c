/***************************************************************************************************/
/* En este ejemplo se puede probar la captura de señales desde la consola                          */
/*                                                                                                 */
/* Con ps se puede ver que la señal está en -ignored- y con noint 2, se puede ver que CTRL-C no    */
/* no funciona.                                                                                    */
/* y por ultimo, se le puede enviar la señal SIGUSR1, para ver como responde                       */
/*                                                                                                 */
/*                                                                                                 */
/* Para ver que señales tiene tomadas/ignoradas se puede ejecutar                                  */
/*     ps -s <pid> / ps -C <nombre-proceso>                                                        */
/*     ps -o pid,ppid,tname,stat,pending,ignored,caught,comm <pid>                                 */
/*                                                                                                 */
/* Para mandar una señal desde otra consola se utiliza el comando kill                             */
/*     kill -<SIGNAL_NAME> <pid>                                                                   */
/*     kill -s <SIGNAL_NAME/SIGNAL_NUMBER> <pid>                                                                   */
/*                                                                                                 */
/***************************************************************************************************/
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
int main (int argc, char * argv[]) {
    if (argc != 2 ) {
        printf ("Error de argumentos\nUso: %s [nro-señal]\n",argv[0]);
        return 1;
    }

	/* Modificamos el handler de la señal para ignorarla.*/

    if (signal(atoi(argv[1]),SIG_IGN) == SIG_ERR) {
        fprintf (stderr, "Error al trapear la señal % d\nError Nro:%i \"%s\"\n", atoi(argv[1]),errno, strerror(errno));
        return 1;
    }

    while (1)
        sleep(1);
    return 0;
}